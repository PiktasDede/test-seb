import express from 'express';

import customersController from '../controllers/customers.controller';
import customersMiddleware from '../middlewere/customers.middleware';

export class CustomersRoutes {

    configure(): express.Router {
        const router = express.Router();

        router.route(`/customers`)
            .get(customersController.getAllCustomers)
            .post(
                customersMiddleware.validateName,
                customersMiddleware.validateSurname,
                customersMiddleware.validateBirthdate,
                customersMiddleware.validatePhone,
                customersMiddleware.validateEmail,
                customersController.createCustomer
            );

        router.route(`/customers/:id`)
            .get(
                customersMiddleware.validateId,
                customersController.getCustomerById,
            );

        return router;
    }

}
