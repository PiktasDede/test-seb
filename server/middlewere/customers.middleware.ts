import express from 'express';

import categoriesService from '../services/customers.service';

class CustomersMiddleware {

    async validateId(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const id = +req.params.id;
        const customer = categoriesService.getCustomerById(id);

        if (!customer) {
            res.status(404).send({
                status: 404,
                error: `Customer does not exists.`
            });
            return;
        }

        next();
    }

    async validateName(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const name = req.body.name;

        if (name.length === 0) {
            res.status(400).send({
                status: 400,
                error: `Customer name is empty.`
            });
            return;
        }

        if (name.length < 1 || name.length > 99) {
            res.status(400).send({
                status: 400,
                error: `Customer name should consist from more than 1 and less than 100 characters.`
            });
            return;
        }

        next();
    }

    async validateSurname(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const surname = req.body.surname;

        if (surname.length === 0) {
            res.status(400).send({
                status: 400,
                error: `Customer surname is empty.`
            });
            return;
        }

        if (surname.length < 1 || surname.length > 99) {
            res.status(400).send({
                status: 400,
                error: `Customer surname should consist from more than 1 and less than 100 characters.`
            });
            return;
        }

        next();
    }

    async validateBirthdate(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const birthdate = req.body.birthdate;
        const bday = new Date(birthdate).setHours(0, 0, 0, 0);
        const today = new Date().setHours(0, 0, 0, 0)

        if (!/^\d{4}\-\d{2}\-\d{2}$/.test(birthdate) || isNaN(bday)) {
            res.status(400).send({
                status: 400,
                error: `Invalid customer birthday format (example: 1978-10-15).`
            });
            return;
        }

        if (today <= bday) {
            res.status(400).send({
                status: 400,
                error: `Customer birth data should be in past.`
            });
            return;
        }

        next();
    }

    async validatePhone(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const phone = req.body.phone;

        if (phone.length === 0) {
            res.status(400).send({
                status: 400,
                error: `Customer phone is empty.`
            });
            return;
        }

        if (!/^[\+\d\-\(\)\.]+$/.test(phone)) {
            res.status(400).send({
                status: 400,
                error: `Invalid customer phone format.`
            });
            return;
        }

        next();
    }

    async validateEmail(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
    ) {
        const email = req.body.email;
        const exp = /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/;

        if (email.length === 0) {
            res.status(400).send({
                status: 400,
                error: `Customer email is empty.`
            });
            return;
        }

        if (!exp.test(email)) {
            res.status(400).send({
                status: 400,
                error: `Invalid customer email format.`
            });
            return;
        }

        next();
    }

}

export default new CustomersMiddleware();
