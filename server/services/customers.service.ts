import Customer from '../models/customer.model';
import customers from './customers.data';

class CustomersService {

    async createCustomer(customer: Omit<Customer, 'id'>): Promise<number> {
        const nextId = customers.length + 1;
        const newCustomer: Customer = { id: nextId, ...customer };

        customers.push(newCustomer);

        return nextId;
    }

    async getAllCustomers(): Promise<Customer[]> {
        return customers;
    }

    async getCustomerById(id: number): Promise<Customer | undefined> {
        return customers.find(customer => customer.id === id);
    }

}

export default new CustomersService();
