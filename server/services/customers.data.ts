import Customer from '../models/customer.model';

let customers: Customer[] = [
    {
        id: 1,
        name: 'Oleg',
        surname: 'Granevskij',
        birthdate: '1988-10-03',
        phone: '+37069652203',
        email: 'o.granevskij@gmail.com'
    },
    {
        id: 2,
        name: 'Elon',
        surname: 'Musk',
        birthdate: '1971-06-28',
        phone: '123456',
        email: 'technoking@tesla.com'
    }
];

export default customers;
