export default interface Customer {
    id: number;
    name: string;
    surname: string;
    birthdate: string;
    phone: string;
    email: string;
}
