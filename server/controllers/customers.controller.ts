import express from 'express';

import customersService from '../services/customers.service';

class CustomersController {

    async getAllCustomers(req: express.Request, res: express.Response) {
        const result = await customersService.getAllCustomers();

        res.status(200).send(result);
    }

    async createCustomer(req: express.Request, res: express.Response) {
        const { name, surname, birthdate, phone, email } = req.body;
        
        const result = await customersService.createCustomer({
            name, surname, birthdate, phone, email
        });

        res.status(200).send({ id: result });
    }

    async getCustomerById(req: express.Request, res: express.Response) {
        const id = +req.params.id;
        
        const result = await customersService.getCustomerById(id);

        res.status(200).send(result);
    }

}

export default new CustomersController();
