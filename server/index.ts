import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { CustomersRoutes } from './routes/customers.routes';

const app = express();
const PORT = 8000;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => res.send('API service works!'));

app.use('/api', new CustomersRoutes().configure());

app.listen(PORT, () => {
  console.log(`[server]: Server is running at http://localhost:${PORT}`);
});
