import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { ApiCustomer } from '../types';
import { CustomersService } from '../services/customers.service';
import { AddCustomer, LoadCustomers, SetFilterQuery } from './customers.actions';

interface CustomersStateModel {
  customers: ApiCustomer[];
  filterQuery: string;
}

@State<CustomersStateModel>({
  name: 'customers',
  defaults: {
    customers: [],
    filterQuery: '',
  }
})
@Injectable()
export class CustomersState {

  constructor(
    private customersService: CustomersService,
  ) {}

  @Selector()
  static getCustomers(state: CustomersStateModel): ApiCustomer[] {
    let result = state.customers;

    // filter if filter string is not empty
    if (state.filterQuery !== '') {
      const exp = new RegExp(`${state.filterQuery}`, 'i');

      result = state.customers.filter(customer => {
        return exp.test(customer.name)
          || exp.test(customer.surname)
          || exp.test(customer.birthdate)
          || exp.test(customer.phone)
          || exp.test(customer.email);
      });
    }

    return result;
  }


  @Action(LoadCustomers)
  loadCustomers({ getState, setState }: StateContext<CustomersStateModel>): any {
    return this.customersService.getCustomers().pipe(
      tap(result => {
        const state = getState();
        setState({
          ...state,
          customers: result.reverse()
        });
      })
    );
  }

  @Action(AddCustomer)
  addCustomer(ctx: StateContext<any>, { payload }: AddCustomer): void {
    ctx.setState((state: CustomersStateModel) => ({ ...state, customers: [payload, ...state.customers] }));
  }

  @Action(SetFilterQuery)
  setFilterQuery(ctx: StateContext<any>, { payload }: SetFilterQuery): void {
    const filterQuery = payload.trim();

    ctx.setState((state: CustomersStateModel) => ({ ...state, filterQuery }));
  }


}
