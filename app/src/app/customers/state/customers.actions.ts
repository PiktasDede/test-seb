import { ApiCustomer } from '../types';

export class LoadCustomers {
  static readonly type = '[Customers] Get';
}

export class AddCustomer {
  static readonly type = '[Customers] Add';

  constructor(public payload: ApiCustomer) {}
}

export class SetFilterQuery {
  static readonly type = '[Customers] Set filter query';

  constructor(public payload: string) {}
}
