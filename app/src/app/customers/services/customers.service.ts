import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { ApiCustomer } from '../types';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {
  private apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient,
  ) { }


  getCustomers(): Observable<ApiCustomer[]> {
    return this.http.get<ApiCustomer[]>(this.apiUrl + '/customers');
  }

  getCustomerById(id: number): Observable<ApiCustomer> {
    return this.http.get<ApiCustomer>(this.apiUrl + `/customers/${id}`);
  }

  addCustomer(body: Omit<ApiCustomer, 'id'>): Observable<{ id: number }> {
    return this.http.post<{ id: number }>(this.apiUrl + '/customers', body);
  }

}
