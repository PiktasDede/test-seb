import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersListComponent } from './components/customers-list/customers-list.component';
import { CustomerDetailsComponent } from './components/customer-details/customer-details.component';
import { CustomerAddComponent } from './components/customer-add/customer-add.component';
import { CustomersFilterComponent } from './components/customers-filter/customers-filter.component';

@NgModule({
  declarations: [
    CustomersListComponent,
    CustomerDetailsComponent,
    CustomerAddComponent,
    CustomersFilterComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    CustomersRoutingModule,
  ]
})
export class CustomersModule { }
