import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { take, tap } from 'rxjs/operators';

import { ApiCustomer } from '../../types';
import { CustomersState } from '../../state/customers.state';
import { LoadCustomers } from '../../state/customers.actions';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.scss']
})
export class CustomersListComponent implements OnInit {

  @Select(CustomersState.getCustomers) customers$!: Observable<ApiCustomer[]>;

  constructor(
    private store: Store,
  ) { }

  ngOnInit(): void {
    this.customers$
      .pipe(
        take(1),
        tap(customers => {
          if (customers.length === 0) {
            this.store.dispatch(new LoadCustomers());
          }
        })
      )
      .subscribe();
  }

}
