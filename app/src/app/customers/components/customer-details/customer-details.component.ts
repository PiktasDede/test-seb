import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';

import { CustomersService } from '../../services/customers.service';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent {
  customer$ = this.route.params
    .pipe(
      switchMap(params => this.customersService.getCustomerById(params.id))
    );

  constructor(
    private route: ActivatedRoute,
    private customersService: CustomersService,
  ) { }

}
