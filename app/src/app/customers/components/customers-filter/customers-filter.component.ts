import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngxs/store';

import { SetFilterQuery } from '../../state/customers.actions';

@Component({
  selector: 'app-customers-filter',
  templateUrl: './customers-filter.component.html',
  styleUrls: ['./customers-filter.component.scss']
})
export class CustomersFilterComponent {
  form = new FormGroup({
    query: new FormControl()
  });

  constructor(
    private store: Store,
  ) { }


  performFilter(): void {
    const { query } = this.form.getRawValue();

    this.store.dispatch(new SetFilterQuery(query));
  }

  clearQuery(): void {
    this.form.controls.query.setValue('');
  }

}
