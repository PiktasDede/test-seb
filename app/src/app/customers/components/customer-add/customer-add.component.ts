import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngxs/store';

import { CustomersService } from '../../services/customers.service';
import { AddCustomer } from '../../state/customers.actions';

interface FormRawValue {
  name: string;
  surname: string;
  birthdate: string;
  phone: string;
  email: string;
}

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html',
  styleUrls: ['./customer-add.component.scss']
})
export class CustomerAddComponent {
  form = new FormGroup({
    name: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
    surname: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
    birthdate: new FormControl(null, [Validators.required, Validators.pattern(/^\d{4}-(0?[1-9]|1[0-2])-(0?[1-9]|1[0-2])$/)]),
    phone: new FormControl(null, [Validators.required]),
    email: new FormControl(null, [Validators.required, Validators.email]),
  });

  error = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store,
    private customersService: CustomersService,
  ) { }


  addCustomer(): void {
    this.error = '';

    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    const formValue = this.form.getRawValue() as FormRawValue;

    this.customersService
      .addCustomer(formValue)
      .subscribe({
        next: res => {
          this.store.dispatch(new AddCustomer({
            id: res.id,
            ...formValue
          }));

          this.router.navigate(['../list'], {
            relativeTo: this.route
          });
        },
        error: err => {
          this.error = err.error.error;
        }
      });
  }

  showError(control: AbstractControl): boolean {
    const { invalid, dirty, touched } = control;
    const showError = invalid && (dirty || touched);

    return showError;
  }

}
