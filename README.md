# Test app

Pilot project

## Instalation

Go into 'server' directory in your terminal, install dependencies and run service on separate process.

```bash
npm install
npm run start:dev
```

After this open new process, change directory to 'app', install dependencies and run project

```bash
npm install
npm start
```

## Usage

In your browser open http://localhost:4200
